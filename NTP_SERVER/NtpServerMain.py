import socket,struct,time,ntplib,sys,os
class NtpServer(object):
    """docstring for NtpServer"""
    RunFlag=True
    def __init__(self):
        super(NtpServer, self).__init__()
    def Run(self):
        BUFSIZE = 1024
        ip_port = ('0.0.0.0', 1234)
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # udp协议
        server.bind(ip_port)
        print("%s * 启动NTP服务,监听端口：UDP-1234。。。\n"%('*'*30))
        while self.RunFlag:
            data,client_addr = server.recvfrom(BUFSIZE)
            try:
                t=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
                unpacked = struct.unpack(ntplib.NTPPacket._PACKET_FORMAT,data[0:struct.calcsize(ntplib.NTPPacket._PACKET_FORMAT)])
                VN=bin(unpacked[0]).replace('0b','')
                VN=int(VN[:-3][-2:],2)
                print(  '%s * 接收到：%s:%s,版本:%s的校时请求\n'%(t,client_addr[0],client_addr[1],VN))
                tx_timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(ntplib.ntp_to_system_time(ntplib._to_time(unpacked[13], unpacked[14]))))
                print("%s * 对方时间："%t,tx_timestamp)
                #实例报文模式
                ntp=ntplib.NTPPacket(VN,mode=4,tx_timestamp=ntplib.system_to_ntp_time(time.time()))
                #生成时间报文
                ntpBackPacket=ntp.to_data()
                 #发送返回报文
                print("\n%s * 发送校对时间："%t,time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))
                server.sendto(ntpBackPacket,client_addr)
            except Exception as e:
                print("\n%s * 接收的是非NTP校时报文:"%t,data)
                t=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
                unpacked = struct.unpack(ntplib.NTPPacket._PACKET_FORMAT,data[0:struct.calcsize(ntplib.NTPPacket._PACKET_FORMAT)])
                VN=bin(unpacked[0]).replace('0b','')
                VN=int(VN[:-3][-2:],2)
                print(  '%s * 接收到：%s:%s,版本:%s的校时请求\n'%(t,client_addr[0],client_addr[1],VN))

        else:
            print(" * 停止 NTP 服务。。。。")
if __name__ == '__main__':
    app=NtpServer()
    os.system("title NTP校时服务")
    app.Run()
