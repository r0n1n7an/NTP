
# -*- coding: utf-8 -*-
# 时间     : 2018-06-09 15:59:19
# 作者     : DL (584054432@qq.com)
# 网址     : https://gitee.com/dingliangPython/
# 软件版本 : Python3.6.5
# 功能     ：
import re,os
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QObject
from configobj import ConfigObj
class Config(QObject):
    '''
        读取info配置文件,默认读取当前目录下config.info,
        可选参数 file,非默认名配置文件路径
    '''
    log=pyqtSignal(str)
    config=pyqtSignal(dict)
    def __init__(self, file=False):
        super(Config,self).__init__()
        self.file=file
        self.log.connect(lambda value:print(value))
    def read(self,encoding='GBK'):
        self.info={}
        #存储配置文件
        if not self.file:
            self.file='./config.ini'
        try:
            file =ConfigObj(os.path.abspath(self.file),encoding=encoding)
            for key in file.keys():
                for k in file[key].keys():

                    if file[key][k].lower()=='true':
                        file[key][k]=True
                    elif file[key][k].lower()=='false':
                        file[key][k]=False
                    # elif file[key][k].isdecimal():
                    #     file[key][k]=int(file[key][k])
                    elif not re.findall('\d+\.\d+\.\d+\.\d+',file[key][k]) and re.findall('\d+\.\d+',file[key][k]):
                        file[key][k]=float(file[key][k])
                    # elif re.findall('\d+x[a-f0-9]+',file[key][k],re.I):
                    #     file[key][k]=hex(int(file[key][k],16))
            self.log.emit("打开配置文件:%s,成功 1"%(os.path.abspath(self.file)))
            self.config.emit(file)
            return file
        except Exception as e:
            self.log.emit("打开配置文件:%s,失败%s 2"%(os.path.abspath(self.file),e))
    def write(self,itemName,keyName,value,encoding='GBK'):
        '''
        write(itemName,keyName,value)
        '''
        try:
            if not self.file:
                    self.file='./config.ini'
            config = ConfigObj(os.path.abspath(self.file),encoding=encoding)
            config[itemName][keyName] = value
            config.write()
            self.log.emit("修改配置%s: %s=%s 成功 3"%(itemName,keyName,value))
            self.config.emit(config)
            return config
        except Exception as e:
            self.log.emit("修改配置%s: %s=%s 失败 ,错误：%s 2"%(itemName,keyName,value,e))
    def addItem(self,itemName,keyName,value,encoding='GBK'):
        try:
            if not self.file:
                self.file='./config.ini'
            config = ConfigObj(os.path.abspath(self.file),encoding=encoding)
            config[itemName] = {}
            config[itemName][keyName] = value
            config.write()
            self.log.emit("增加配置%s: %s=%s 成功 3"%(itemName,keyName,value))
            self.config.emit(config)
            return config
        except Exception as e:
            self.log.emit("增加配置%s: %s=%s 失败  ,错误：%s 2"%(itemName,keyName,value,e))
    def delItem(self,itemName,keyName=None,encoding='GBK'):
        '''
            delItem(self,itemName,keyName=None)
        '''
        try:
            if not self.file:
                self.file='./config.ini'
            config = ConfigObj(os.path.abspath(self.file),encoding=encoding)
            if config[itemName] and config[itemName][keyName]:
                del config[itemName][keyName]
                self.log.emit("删除配置：%s[%s] 成功 3"%(itemName,keyName))
                self.config.emit(config)
                config.write()
                return config
            else :
                del config[itemName]
                self.log.emit("删除配置：%s 成功 3"%(itemName))
                self.config.emit(config)
                config.write()
                return config
        except Exception as e:
            try:
                config[itemName]
                self.log.emit("删除配置文件 %s: %s 失败  ,错误：%s 2"%(itemName,keyName,e))
            except Exception as e:
                self.log.emit("删除配置文件 %s 不存在 错误：%s 3"%(itemName,e))
    def asSave(self,dictTable,filePath=None,encoding='GBK'):
        if not filePath:
                    filePath='./config.ini'

        config=ConfigObj(filePath,encoding=encoding)
        for key in dictTable.keys():
            config[key]=dictTable[key]
        config.write()
        self.log.emit("另存配置文件成功 1")

if __name__=="__main__":
    #filename=r'.\gcl_sdk\config\apps.config.ini'
    app=Config()
    print(app.read())
    app.write('ShareMemory','YcaddCount',20480)
    # a={'xxx':{'aa':111,'bb':True}}
    app.delItem('xxx','dd')
