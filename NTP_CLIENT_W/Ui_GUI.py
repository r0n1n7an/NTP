# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\ICS\Python\SCR\NTP\GUI.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(379, 119)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/clock32.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setWindowOpacity(2.0)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(171, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 0, 2, 3)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 0, 3, 1, 2)
        self.NowTime = QtWidgets.QLabel(self.centralwidget)
        self.NowTime.setObjectName("NowTime")
        self.gridLayout.addWidget(self.NowTime, 0, 5, 1, 2)
        spacerItem1 = QtWidgets.QSpacerItem(63, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 1, 4, 2, 2)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.NtpAddress = QtWidgets.QLineEdit(self.centralwidget)
        self.NtpAddress.setAlignment(QtCore.Qt.AlignCenter)
        self.NtpAddress.setObjectName("NtpAddress")
        self.gridLayout.addWidget(self.NtpAddress, 2, 1, 1, 3)
        self.AutoMode = QtWidgets.QCheckBox(self.centralwidget)
        self.AutoMode.setCheckable(True)
        self.AutoMode.setChecked(True)
        self.AutoMode.setTristate(False)
        self.AutoMode.setObjectName("AutoMode")
        self.gridLayout.addWidget(self.AutoMode, 2, 6, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 3, 0, 1, 1)
        self.Cycle = QtWidgets.QSpinBox(self.centralwidget)
        self.Cycle.setMouseTracking(False)
        self.Cycle.setAcceptDrops(False)
        self.Cycle.setAutoFillBackground(False)
        self.Cycle.setWrapping(False)
        self.Cycle.setAccelerated(False)
        self.Cycle.setProperty("showGroupSeparator", False)
        self.Cycle.setMaximum(604800)
        self.Cycle.setSingleStep(5)
        self.Cycle.setProperty("value", 10)
        self.Cycle.setObjectName("Cycle")
        self.gridLayout.addWidget(self.Cycle, 3, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 3, 2, 1, 1)
        self.Manual = QtWidgets.QPushButton(self.centralwidget)
        self.Manual.setObjectName("Manual")
        self.gridLayout.addWidget(self.Manual, 3, 3, 1, 3)
        self.SaveConfig = QtWidgets.QPushButton(self.centralwidget)
        self.SaveConfig.setObjectName("SaveConfig")
        self.gridLayout.addWidget(self.SaveConfig, 3, 6, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Ntp自动校时"))
        self.label_4.setText(_translate("MainWindow", "当前时间："))
        self.NowTime.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff5500;\">2018-06-10 18:00:00</span></p></body></html>"))
        self.label_3.setText(_translate("MainWindow", "NTP服务器："))
        self.NtpAddress.setToolTip(_translate("MainWindow", "<html><head/><body><p>校时服务器IP,或域名</p></body></html>"))
        self.NtpAddress.setText(_translate("MainWindow", "ntp1.aliyun.com"))
        self.AutoMode.setToolTip(_translate("MainWindow", "<html><head/><body><p>选中，会按校时周期自动校时</p></body></html>"))
        self.AutoMode.setText(_translate("MainWindow", "自动校时"))
        self.label.setText(_translate("MainWindow", "校时周期："))
        self.Cycle.setToolTip(_translate("MainWindow", "<html><head/><body><p>自动校时周期</p></body></html>"))
        self.label_2.setText(_translate("MainWindow", "秒"))
        self.Manual.setToolTip(_translate("MainWindow", "<html><head/><body><p>立即连网校时</p></body></html>"))
        self.Manual.setText(_translate("MainWindow", "手动校时"))
        self.SaveConfig.setText(_translate("MainWindow", "保存配置"))

import ntp_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

